package com.cs.bru;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeepWordTeachV3Application {

	public static void main(String[] args) {
		SpringApplication.run(KeepWordTeachV3Application.class, args);
	}
}
